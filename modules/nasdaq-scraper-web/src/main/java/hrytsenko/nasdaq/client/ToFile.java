package hrytsenko.nasdaq.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import hrytsenko.nasdaq.client.data.ClientCompany;

public final class ToFile {

	public static String toCSV(List<ClientCompany> companies) {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(ClientCompany.class).withHeader();
		String str = "";
		try {
			str = mapper.writer(schema).writeValueAsString(companies);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;

	}

	private static final Object[] FIRST_LINE = { "exchange", "name", "sector", "subsector", "symbol" };

	public static byte[] toXLSX(List<ClientCompany> companies) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		try (Workbook workbook = new HSSFWorkbook();) {
			Sheet sheet = workbook.createSheet("Companies");
			toLine(sheet.createRow(0), FIRST_LINE);
			for (int i = 0; i < companies.size(); i++) {
				ClientCompany company = companies.get(i);
				toLine(sheet.createRow(i + 1), company.getExchange(), company.getName(), company.getSector(),
						company.getSubsector(), company.getSymbol());
			}
			workbook.write(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b.toByteArray();
	}

	public static void toLine(Row row, Object... values) {
		for (int i = 0; i < values.length; i++) {
			row.createCell(i).setCellValue(String.valueOf(values[i]));
		}
	}
}
