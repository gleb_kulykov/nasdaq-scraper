package hrytsenko.nasdaq.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.common.base.Strings;

import hrytsenko.nasdaq.client.data.ClientCompany;
import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.error.ApplicationException;

@Stateless
@Path("companies")
@Produces(MediaType.APPLICATION_JSON)
public class CompaniesRestService {

    @Inject
    private CompaniesService companiesService;

    @GET
    public List<ClientCompany> findCompanies(@QueryParam("exchange") String exchange,
            @QueryParam("symbol") String symbol, @QueryParam("sector") String sector) {
        return companiesService.findCompanies(optionalOf(exchange), optionalOf(symbol), optionalOf(sector)).stream()
                .map(ClientCompany::fromCompany).collect(Collectors.toList());
    }

    private Optional<String> optionalOf(String parameter) {
        return Optional.ofNullable(Strings.emptyToNull(parameter));
    }

    @GET
    @Path("sectors")
    public List<String> findSectors() {
        return companiesService.findSectors();
    }
    
    @GET
	@Path("/csv")
	@Produces("text/csv")
    public Response getCSVFile(@QueryParam("exchange") String exchange, @QueryParam("symbol") String symbol,
			@QueryParam("sector") String sector) {
		List<ClientCompany> companies = companiesService.findCompanies(optionalOf(exchange), optionalOf(symbol), optionalOf(sector)).stream()
                .map(ClientCompany::fromCompany).collect(Collectors.toList());
		String timestamp = new SimpleDateFormat("dd-MM-yy_HH-mm-ss").format(new Date());
		return attachmentResponse(ToFile.toCSV(companies), "Companies_" + timestamp + ".csv");
	}
    @GET
    @Path("/xlsx")
    @Produces("application/vnd.ms-excel")
    public Response getXLSXFile(@QueryParam("exchange") String exchange, @QueryParam("symbol") String symbol,
			@QueryParam("sector") String sector){
    	List<ClientCompany> companies = companiesService.findCompanies(optionalOf(exchange), optionalOf(symbol), optionalOf(sector)).stream()
                .map(ClientCompany::fromCompany).collect(Collectors.toList());
    	String timestamp = new SimpleDateFormat("dd-MM-yy_HH-mm-ss").format(new Date());
    	return attachmentResponse(ToFile.toXLSX(companies),"Companies_" + timestamp + ".xlsx");
    }
    
    
    
    private Response attachmentResponse(String str, String name){
    	try {
			name = URLEncoder.encode(name, "UTF-8");
		} catch (UnsupportedEncodingException exception) {
			throw new ApplicationException("Could not encode filename.", exception);
		}
		return Response.ok(str).header("Content-Disposition", "attachment; filename=\"" + name + "\"").build();
    }
    
    private Response attachmentResponse(byte[] b, String name){
    	try {
			name = URLEncoder.encode(name, "UTF-8");
		} catch (UnsupportedEncodingException exception) {
			throw new ApplicationException("Could not encode filename.", exception);
		}
		return Response.ok(b).header("Content-Disposition", "attachment; filename=\"" + name + "\"").build();
    }
}
