package hrytsenko.nasdaq.daemon;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;
import hrytsenko.nasdaq.endpoint.data.NasdaqCompany;

public class NasdaqServiceTest {

    private static final NasdaqCompany AAPL = new NasdaqCompany("AAPL", "Apple Inc.", "Technology",
            "Computer Manufacturing");
    private static final NasdaqCompany MSFT = new NasdaqCompany("MSFT", "Microsoft Corporation", "Technology",
            "Computer Software: Prepackaged Software");
    private static final NasdaqCompany ORCL = new NasdaqCompany("ORCL", "Oracle Corporation", "Technology",
            "Computer Software: Prepackaged Software");

    @Mock
    private CompaniesService companiesService;
    @Mock
    private NasdaqEndpoint nasdaqEndpoint;

    @InjectMocks
    private NasdaqService nasdaqService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        Mockito.doReturn(Arrays.asList(NasdaqCompany.toCompany(AAPL), NasdaqCompany.toCompany(MSFT)))
                .when(nasdaqEndpoint).downloadCompanies("NASDAQ");
        Mockito.doReturn(Arrays.asList(NasdaqCompany.toCompany(ORCL))).when(nasdaqEndpoint).downloadCompanies("NYSE");
    }

    @Test
    public void testUpdateCompanies() {
        nasdaqService.updateCompanies(Arrays.asList("NASDAQ", "NYSE"));

        Mockito.verify(companiesService, Mockito.times(3)).updateCompany(Matchers.any(Company.class));
        Mockito.verify(nasdaqEndpoint, Mockito.times(2)).downloadCompanies(Matchers.anyString());
        Mockito.verifyNoMoreInteractions(companiesService, nasdaqEndpoint);
    }

}
